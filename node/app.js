const express = require('express')
var path = require('path');
var bodyParser = require('body-parser');
const Excel = require('exceljs');
const fs = require('fs');
const app = express()
const port = 3000

app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


app.all('/*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,enctype,authorization');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});
app.set('views', path.join(__dirname, 'views'));

app.post('/saveData', (req, res) => {
	  // Create workbook & add worksheet
	  let workbook = new Excel.Workbook();
//const worksheet = workbook.addWorksheet('COE_courseDetails');

var xlFilePath="D:\\mini_project\\poc\\getting-started-with-exceljs\\COE_courseDetails.xlsx";
    //Read xlsx file and use then fuction to handle promise before executing next step
    workbook.xlsx.readFile(xlFilePath).then(function () {
        var worksheet = workbook.getWorksheet("COE Details");
        let rowavailable=true;
		worksheet.eachRow(function(row, rowNumber) {
			if(rowavailable){
			rowavailable=rowNumber;	
			let currentrow = worksheet.getRow(rowNumber);
			if(currentrow.getCell(3).value === req.body.course && currentrow.getCell(2).value === req.body.email){
			rowavailable=false;	
			}
			}
        
			
   // console.log('Row ' + rowNumber + ' = ' + JSON.stringify(row.values));
    //Do whatever you want to do with this row like inserting in db, etc
		});
console.log('Row ' + rowavailable)
if(rowavailable){
	console.log('Row ' + rowavailable)
let newrow = worksheet.getRow(rowavailable+1);
 newrow.getCell(1).value = req.body.name;
 newrow.getCell(2).value = req.body.email;
 newrow.getCell(3).value = req.body.course;
 newrow.getCell(4).value = new Date();
 
        newrow.commit();
		workbook.xlsx.writeFile("newsample.xlsx");
		fs.unlinkSync(xlFilePath);
    fs.rename("D:\\mini_project\\poc\\getting-started-with-exceljs\\newsample.xlsx", xlFilePath, function(err) {
    if ( err ) res.send(err);
     });    
	   res.json({
                "message": "saved",
                "status": 200
            })
}else{
	res.json({
                "message": "Duplicate record",
                "status": 200
            })

}

        
    }) 
   // var filePath = "sample.xlsx"; 
    
//res.send('saved');
    
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})