import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-manageorders',
  templateUrl: './manageorders.component.html',
  styleUrls: ['./manageorders.component.scss']
})
export class ManageordersComponent implements OnInit {
  ordersForm: FormGroup;
  loading = false;
  error = '';
  name = 'Angular';
  radioSelected: any;
  result:any;
  courses = [
    {
      name:'Fullstack',
      value:'Full Stack'
   },
   {
       name:'QualityEngineering',
       value:'Quality Engineering'
    },
    {
        name:'Integration',
        value:'Integration'
     },
     {
         name:'DataofMotion',
         value:'Data of Motion'
      },
      {
          name:'IntelligentAutomation',
          value:'Intelligent Automation'
       },
      {
          name:'DataManagement',
          value:'Data Management'
       },
      {
          name:'Package',
          value:'Package (SAP + Oracle)'
       },
       {
           name:'Oraclecloud',
           value:'Oracle Cloud'
        }
  ]

  constructor(
      private formBuilder: FormBuilder,  
      private authenticationService: AuthenticationService,
      

  ) {}

  ngOnInit() {
      this.ordersForm = this.formBuilder.group({
          name: ['', Validators.required],
          course: ['', Validators.required],
          email: ['', Validators.required],
      });
      
  }

  onSubmit() {
      // stop here if form is invalid
      if (this.ordersForm.invalid) { 
          return;
      }
      let data={
        name:this.ordersForm.value.name,
        course:this.ordersForm.value.course,
        email:this.ordersForm.value.email
      }
      console.log(data)
      this.authenticationService.addCourse('http://localhost:3000/saveData',data).then(response => {
      console.log("component"+response) 
      this.result=response.message;
      },
        error => console.log(error));	
        }
         
  }
  


