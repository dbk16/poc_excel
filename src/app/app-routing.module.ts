import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageordersComponent } from './components/manageorders/manageorders.component';



const routes: Routes = [
  { path: '', component: ManageordersComponent},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
