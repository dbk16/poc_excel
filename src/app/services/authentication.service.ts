import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { throwError, ObservableInput } from "rxjs";
import { catchError, retry } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) {}

	private handleError(error: HttpErrorResponse): ObservableInput<ErrorEvent> {
		console.log(error);
		if (error.error instanceof ErrorEvent) {
			// Handler network error
			console.log(error.message);
			retry(2);
		} else {
			console.log(error.status);
			return throwError(error.error);
		}
		return throwError(error.message);
	}

	

	public addCourse(url: string, body: any, options: any = {}): Promise<any> {
		return this.http.post(url, body, options).pipe(catchError(this.handleError)).toPromise();
	}

	
}
